import random
import time

rockpaperscissors = 'Rock! Paper! Scissors!'.split()

def intro():
    print('Welcome to a rock paper scissors game!')
    input('Press enter when you are ready to start!')
    print('Get ready! The game starts in...')
    time.sleep(1)
    print('3...')
    time.sleep(1)
    print('2...')
    time.sleep(1)
    print('1...')
    time.sleep(1)
    print('Rock, paper, scissors, shoot!')

def computerAnswer(y):
    altlist = random.randint(0,len(y) - 1)
    return(y[altlist])

def getAnswer():
    print('(Enter rock, paper, or scissors)')
    x = input()
    x = x.lower()
    if x not in 'rockpaperscissors':
        print('Please enter a valid answer')
    else:
        return(x)

def compareAnswers(answer, companswer):
    answer = answer[0]

    if answer == 'r' and companswer == 'Rock!':
        print("It's a tie!")
    elif answer == 's' and companswer == 'Rock!':
        print('You lose!')
    elif answer == 'p' and companswer == 'Rock!':
        print('You win!')

    elif answer == 'r' and companswer == 'Paper!':
        print('You lose!')
    elif answer == 's' and companswer == 'Paper!':
        print('You win!')
    elif answer == 'p' and companswer == 'Paper!':
        print("It's a tie!")

    elif answer == 'r' and companswer == 'Scissors!':
        print('You win!')
    elif answer == 's' and companswer == 'Scissors!':
        print("It's a tie!")
    elif answer == 'p' and companswer == 'Scissors!':
        print("You lose!")

    else:
        print('Something went wrong')

playAgain = True
while playAgain:
    intro()
    computerAnswer(rockpaperscissors)
    compareAnswers(getAnswer(), computerAnswer(rockpaperscissors))

    print('Would you like to play again?')
    answer = input()
    if answer == 'no':
        break
