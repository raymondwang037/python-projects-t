import sys
import math

def diamondArea():


    method = int(input('Which data values do you have? 1. Diagonals, 2. Altitude and Base, 3. Side and Angle. Enter the number of your selected method. '))
    if method == 1:

        d1 = int(input('Input diagonal 1 '))
        d2 = int(input('Input diagonal 2 '))

        e1 = d1 * d2
        ans1 = e1 / 2
        ans9 = str(ans1)

        print('Your area is ' + ans9)

    elif method == 2:

        alt = int(input('Altitude? '))
        base = int(input('Base? '))
        pd = alt * base
        pd1 = str(pd)

        print('Your area is ' + pd1)

    elif method == 3:

        side = int(input('Side?'))
        angle = int(input('Angle in degrees?'))

        side2 = side*side
        rangle = angle * (math.pi/180)
        sangle = math.sin(rangle)
        ans2 = sangle*side2
        ans3 = str(ans2)

        print ('Your area is ' + ans3)
    else:
        print('error, check entry for mistakes')

diamondArea()
