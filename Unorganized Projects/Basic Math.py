#Raymond wang
import math

def Addition():

    print("Input number A")
    numberA = float(input())
    print("Input number B")
    numberB = float(input())

    numberC = numberA + numberB
    return numberC

def Division():

    numberA = float(input("Input number A "))
    numberB = float(input("Input number B "))

    numberC = numberA / numberB
    return numberC

def Multiplication():

    numberA = float(input("Input number A "))
    numberB = float(input("Input number B "))

    numberC = numberA * numberB
    return numberC

def Subtraction():

    numberA = float(input("Input original number "))
    numberB = float(input("Input number to be subtracted "))

    numberC = numberA - numberB
    return numberC

choice = int(input('''What would you like to do? 
1. Addition 
2. Subtraction
3. Division
4. Multiplication
'''))
if choice == 1:
    print(Addition())
elif choice == 2:
    print(Subtraction())
elif choice == 3:
    print(Division())
elif choice == 4:
    print(Multiplication())