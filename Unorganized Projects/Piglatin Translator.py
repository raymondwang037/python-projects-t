VOWELS = 'AEIOU'


def whichTranslator():
    print('Would you like to 1.Translate english into piglatin, or 2.Translate piglatin into english (1 or 2)')
    choice = int(input())
    if choice not in range(1,3):
        print('please enter either 1 or 2')
    else:
        return int(choice)


def translateIntoPiglatin():
    print('Please enter a sentence in english')
    sentence = input().split()
    psentence = []

    for n in range(len(sentence)):
        word = list(sentence[n])
        if word[0].upper() in VOWELS:
            word = ''.join(word)
            word = word + 'yay'
            psentence.append(word)
        elif word[0].upper() not in VOWELS and word[1].upper() not in VOWELS:
            word0 = word[0]
            word1 = word[1]
            word = word[2:]
            word = ''.join(word)
            word = word + word0 + word1 + 'ay'
            psentence.append(word)
        elif word[0].upper() not in VOWELS:
            word0 = word[0]
            word.remove(word[0])
            word = ''.join(word)
            word = word + word0 + 'ay'
            psentence.append(word)

    return ' '.join(psentence)

def translateIntoEnglish():
    print('Please enter a sentence in piglatin')
    sentence = input().split()
    esentence = []

    for n in range(len(sentence)):
        word = list(sentence[n])
        lima = len(word)
        if ''.join(word) == 'ouyay':
            word = 'you'
            esentence.append(word)
        elif word[lima-3:] == 'yay':
            word = word[:lima-3]
            word = ''.join(word)
            esentence.append(word)
        elif word[lima-4].upper() not in VOWELS and word[lima-3].upper() not in VOWELS:
            word0 = word[lima-4]
            word1 = word[lima-3]
            word = word[:lima-4]
            word = ''.join(word)
            word = word0 + word1 + word
            esentence.append(word)
        elif word[lima-3].upper() not in VOWELS:
            word0 = word[lima-3]
            word = word[:lima-3]
            word = ''.join(word)
            word = word0 + word
            esentence.append(word)

    return ' '.join(esentence)

x = whichTranslator()
if x == 1:
    print(translateIntoPiglatin())
elif x == 2:
    print(translateIntoEnglish())
