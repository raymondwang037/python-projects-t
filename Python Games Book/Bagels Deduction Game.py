import random
NUM_AMOUNT = 3
MAX_GUESS = 10

def getRandomNumber():
    numberList = list(range(10))
    random.shuffle(numberList)
    secretNum = ''
    for x in range(NUM_AMOUNT):
        secretNum += str(numberList[x])
        return secretNum

def getClues(guess, secretNum):
    if guess == secretNum:
        print('You got it!')

    clues = []

    for x in range(len(guess)):
        if guess[x] == secretNum[x]:
            clues.append('Fermi')
        elif guess[x] in secretNum:
            clues.append('Pico')
        else:
            return 'Bagels'

    clues.sort()
    return ' '.join(clues)

print('I am thinking of a %s-digit number. Try to guess what it is' %NUM_AMOUNT)
print('When I say:        That means:')
print(' Bagels             None of the digits is correct.')
print(' Pico               One digit is correct but in the wrong position.')
print(' Fermi              One digit is correct and in the right position.')


