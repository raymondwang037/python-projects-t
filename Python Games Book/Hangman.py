import random
words = 'ant baboon badger bat bear beaver camel cat clam cobra cougar coyote crow deer dog donkey duck eagle ferret fox frog goat goose hawk lion lizard llama mole monkey moose mouse mule newt otter owl panda parrot pigeon python rabbit ram rat raven rhino salmon seal shark sheep skunk sloth snake spider stork swan tiger toad trout turkey turtle weasel whale wolf wombat zebra'.split()
GUESSESREMAINING = ['6/6 guesses remain', '5/6 guesses remain', '4/6 guesses remain', '3/6 guesses remain', '2/6 guesses remain', '1/6 guesses remain']

def getRandomWord(wordList):
    wordIndex = random.randint(0, len(wordList) - 1)
    return wordList[wordIndex]

def displayBoard(missedLetters, correctLetters, secretWord):
    print(GUESSESREMAINING[len(missedLetters)])

    print('Missed letters:', end=' ')
    for letter in missedLetters:
        print(letter, end=' ')
    print()

    blanks = '_' * len(secretWord)

    for x in range(len(secretWord)):
        if secretWord[x] in correctLetters:
            blanks = blanks[:x] + secretWord[x] + blanks[x+1:]

    for letter in blanks:
        print(letter, end=' ')
    print()

def getGuess(alreadyGuessed):
    while True:
        print('Guess a letter')
        guess = input()
        guess = guess.lower()
        if len(guess) != 1:
            print('Please enter a single letter')
        elif guess not in 'abcdefghijklmnopqrstuvwxyz':
            print('Please enter a LETTER')
        elif guess in alreadyGuessed:
            print(' You have already guessed this letter, please choose a different letter')
        else:
            return guess

def playAgain():
    print('Do you want to play again?')
    return input().lower().startswith('y')

print('H A N G M A N')
missedLetters = ' '
correctLetters = ' '
secretWord = getRandomWord(words)
gameisdone = False

while True:
    displayBoard(missedLetters, correctLetters, secretWord)

    guess = getGuess(missedLetters+correctLetters)

    if guess in secretWord:
        correctLetters = correctLetters + guess

        foundAllLetters = True
        for x in range(len(secretWord)):
            if secretWord[x] not in correctLetters:
                foundAllLetters = False
                break
        if foundAllLetters:
            print('Yes! the word was ' + secretWord + '! You have won!')
            gameisdone = True
    else:
        missedLetters = missedLetters + guess
        if len(missedLetters) == len(GUESSESREMAINING) - 1:
            displayBoard(missedLetters, correctLetters, secretWord)
            print('You have run out of guesses!\nAfter ' + str(len(missedLetters)) + ' missed guesses and ' + str(len(correctLetters)) + ' correct guesses, the word was: ' + secretWord)
            gameisdone = True

    if gameisdone == True:
        if playAgain():
            missedLetters = ''
            correctLetters = ''
            gameisdone = False
            secretWord = getRandomWord(words)
        else:
            break
