import random
computerLetter = 'X'
playerLetter = 'O'


def intro():
    print('Welcome to Tic Tac Toe, the hardest game to code so far; now features AI')
    print('To get started, please hit enter.')
    input()


def whoGoesFirst():
    if random.randint(0,1) == 0:
        return 'computer'
    else:
        return 'player'


def drawBoard(board):
    print(board[7] + '|' + board[8] + '|' + board[9])
    print('-+-+-')
    print(board[4] + '|' + board[5] + '|' + board[6])
    print('-+-+-')
    print(board[1] + '|' + board[2] + '|' + board[3])


def makeMove(board, move, letter):
    board[move] = letter


def isSpaceFree(board, move):
    return board[move] == ' '


def getPlayerMove(board):
    print('What is your move? (1-9)')
    move = input()
    while move not in '1 2 3 4 5 6 7 8 9'.split() or not isSpaceFree(board, int(move)):
        print('Please enter a valid move (1-9)')
        move = input()
    return int(move)


def getBoardCopy(board):
    boardCopy = []
    for i in board:
        boardCopy.append(i)
    return boardCopy


def isWinner(b, l):
    return (b[7] == l and b[8] == l and b[9] == l) or (b[4] == l and b[5] == l and b[6] == l) or (b[1] == l and b[2] == l and b[3] == l) or (b[7] == l and b[4] == l and b[1] == l) or (b[8] == l and b[5] == l and b[2] == l) or (b[9] == l and b[6] == l and b[3] == l) or (b[7] == l and b[5] == l and b[3] == l) or (b[1] == l and b[5] == l and b[9] == l)


def getComputerMove(board, computerLetter):
    for x in range(1, 10):
        boardCopy = getBoardCopy(board)
        if isSpaceFree(boardCopy, x):
            makeMove(boardCopy, x, computerLetter)
            if isWinner(boardCopy, computerLetter):
                return x

    for x in range(1, 10):
        boardCopy = getBoardCopy(board)
        if isSpaceFree(boardCopy, x):
            makeMove(boardCopy, x, playerLetter)
            if isWinner(boardCopy, playerLetter):
                return x

    w = [1,3,7,9]
    random.shuffle(w)

    for y in w:
        boardCopy = getBoardCopy(board)
        if isSpaceFree(boardCopy, y):
            return y

    if isSpaceFree(board, 5):
        return 5


def isBoardFull(board):
    for i in range(1,10):
        if isSpaceFree(board, i):
            return False
    return True


intro()


while True:
    theBoard = [' '] * 10
    turn = whoGoesFirst()
    print('The ' + turn + ' will go first.')
    gameIsPlaying = True
    while gameIsPlaying:
        if turn == 'player':
            drawBoard(theBoard)
            print("It is the player's turn")
            move = getPlayerMove(theBoard)
            makeMove(theBoard, move, playerLetter)

            if isWinner(theBoard, playerLetter):
                drawBoard(theBoard)
                print("You have won!")
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard) == False:
                    turn = 'computer'
                else:
                    print('The game is a tie!')
                    gameIsPlaying = False

        else:
            move = getComputerMove(theBoard, computerLetter)
            makeMove(theBoard, move, computerLetter)

            if isWinner(theBoard, computerLetter):
                drawBoard(theBoard)
                print('The computer has beaten you! You lose!')
                gameIsPlaying = False
            else:
                if isBoardFull(theBoard) == False:
                    turn = 'player'
                else:
                    print('The game is a tie!')
                    gameIsPlaying = False

    print('Do you want to play again?')
    if not input().lower().startswith('y'):
        break
