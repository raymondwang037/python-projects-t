#setup
import sys
import random
number = random.randint(1,20)
guessesTaken = 0

#name asking
name = input('What is your name? ')
print ('Hello ' + name )

game = input('Do you wish to play a game? (yes/no) ')
game = str(game)

#do you want to play
if game == 'no':
    sys.exit('Goodbye')
if game == 'yes':
    print ('Great! I am thinking of a number between 1 and 20 (enter numbers)')

for guessesTaken in range (1,6):
    guess = input()
    guess = int(guess)

    if guess > number:
        print ('Too high, guess lower.')

    if guess < number:
        print ('Too low, guess higher.')

    if guess == number:
        break

if guess == number:
    guessesTaken = str(guessesTaken + 1)
    print ('You have won. You took: '+guessesTaken+' guesses.')

if guess != number:
    number = str(number)
    print ('You have failed to guess my number. The number was '+number)