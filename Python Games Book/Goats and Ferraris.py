import random
import time

def intro():
    print()
    print()
    print('Welcome to "Goats and Ferraris!" The only game show where you have a chance to win a goat or a Ferrari!')
    print()
    time.sleep(1)
    print('There are three doors in front of you, two of them contain goats, and one of them contains a Ferrari.')
    print()
    time.sleep(1)

def pickdoor():
    doorchosen = ''
    while doorchosen != '1' and doorchosen != '2':
        print('Will you choose door #1, door #2, or door #3? (Enter number)')
        doorchosen = str(input())

    return doorchosen


def whichdoor(X):
    X = str(X)
    print('You have chosen door #' + X)
    print()
    time.sleep(1)
    print('Behind door #' + X + ' is...')
    print()
    time.sleep(1)

    ferraridoor = random.randint(1,3)

    if X == str(ferraridoor):
        print('A Ferrari!')
    else:
        print('A Goat!')

playAgain = 'yes'
while playAgain == 'yes':
    intro()
    Y = pickdoor()
    whichdoor(Y)

    print('Do you want to try again? (yes or no)')
    playAgain = input()
